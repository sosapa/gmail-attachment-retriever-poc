// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve

class GmailEmailAttachment {
  String name
  String contentType
  byte[] data
}
