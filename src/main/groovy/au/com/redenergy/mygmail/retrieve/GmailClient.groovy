// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve

import com.google.api.client.googleapis.json.GoogleJsonResponseException
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.ListMessagesResponse
import com.google.api.services.gmail.model.Message
import groovy.util.logging.Slf4j
import org.apache.http.HttpStatus

import javax.mail.internet.MimeMessage

@Slf4j
class GmailClient {
  static final String DEFAULT_USER = "me"
  GoogleMailClientFactory clientFactory = new BatchGoogleMailClientFactory()
  Gmail gmail
  String userid = DEFAULT_USER

  GmailClient(Map<String, Object> options) {
    userid = options['username']
    gmail = clientFactory.makeClient(options['clientId'], options['clientSecret'], options['applicationName'], options['refreshToken'], options['accessToken'])
  }

  void deleteMessage(Message message) throws IOException {
    log.info("Deleting message ${message.getId()}")
    try {
      gmail.users().messages().delete(userid, message.getId()).execute()
    } catch (GoogleJsonResponseException e) {
      if(e.statusCode == HttpStatus.SC_NOT_FOUND) {
        log.info("Unable to delete: ${message}", e)
      }
      else {
        throw e
      }
    }
  }

  void deleteMessage(String user) throws IOException {
    log.info("Deleting messages for user ${user} (${gmail.users()})")

    ListMessagesResponse messagesResponse = gmail.users().messages().list(user).execute()
    log.info("List of messages for user [${user}(id: ${userid})] is: [${messagesResponse}]")
    delete(messagesResponse)
  }

  private void delete(ListMessagesResponse messagesResponse) {
    if (Objects.nonNull(messagesResponse.getMessages()) & messagesResponse.getMessages()?.size() > 0) {
      List<Message> messages = messagesResponse.getMessages()
      messages.each {
        try {
          gmail.users().messages().delete(userid, it.getThreadId()).execute()
        }
        catch (Throwable t) {
          //Ignore
          log.warn("Unable to delete message: [${it}]", t)
        }
      }
      // TODO-PS Why the recursion here?
//      delete(gmail.users().messages().list(userid).execute())
    }
  }

  Message sendEmailWithAttachmentAndBody(File file, String body) {
    log.info("Sending email with attachemnt ${file.name}")
    if (!file.exists()) {
      throw new IllegalArgumentException("Can't find file ${file.name} ")
    }
    return sendEmailWithAttachmentAndBody(body, file.text.bytes, body)
  }

  Message sendEmailWithAttachmentAndBody(String fileName, byte[] attachmentContent, String body) {
    log.info("Sending email with attachemnt ${fileName}")
    MimeMessage message = clientFactory.createEmailWithAttachment(userid, userid, fileName, body, [(fileName): attachmentContent])
    return clientFactory.sendMessage(gmail, userid, message)
  }

  Message sendEmailWithMultipleAttachmentsAndBody(File file1, File file2, String body) {
    log.info("Sending email with attachemnt ${file1.name} and ${file2.name}")
    if (!file1.exists()) {
      throw new IllegalArgumentException("Can't find file ${file1.name} ")
    }
    if (!file2.exists()) {
      throw new IllegalArgumentException("Can't find file ${file2.name} ")
    }
    return sendEmailWithMultipleAttachmentsAndBody([(file1.name): file1.text.bytes, (file2.name): file2.text.bytes], body)
  }

  Message sendEmailWithMultipleAttachmentsAndBody(Map<String, byte[]> attachments, String body) {
    log.info("Sending email with attachment ${attachments.keySet()}")
    MimeMessage message = clientFactory.createEmailWithAttachment(userid, userid, attachments.keySet().join(" AND "), body, attachments)
    return clientFactory.sendMessage(gmail, userid, message)
  }

}
