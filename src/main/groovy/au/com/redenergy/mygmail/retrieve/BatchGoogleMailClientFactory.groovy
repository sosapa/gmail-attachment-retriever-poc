// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.googleapis.auth.oauth2.GoogleCredential
import com.google.api.client.http.HttpRequest
import com.google.api.client.http.HttpRequestInitializer
import com.google.api.client.http.javanet.NetHttpTransport
import com.google.api.client.json.jackson2.JacksonFactory
import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.Message

import javax.activation.DataHandler
import javax.activation.DataSource
import javax.mail.MessagingException
import javax.mail.Multipart
import javax.mail.Session
import javax.mail.internet.InternetAddress
import javax.mail.internet.MimeBodyPart
import javax.mail.internet.MimeMessage
import javax.mail.internet.MimeMultipart
import javax.mail.util.ByteArrayDataSource

class BatchGoogleMailClientFactory implements GoogleMailClientFactory {

  static final String MIME_TYPE_GMAIL_TEST = "text/plain"
  static final String MIME_TYPE_GMAIL_ZIP_TEST = "application/zip"
  private NetHttpTransport transport
  private JacksonFactory jsonFactory

  int connectTimeoutMillis = 60000
  int readTimeoutMillis = 60000

  BatchGoogleMailClientFactory() {
    this.transport = new NetHttpTransport()
    this.jsonFactory = new JacksonFactory()
  }

  @Override
  Gmail makeClient(String clientId, String clientSecret, String applicationName, String refreshToken, String accessToken) {
    if (clientId == null || clientSecret == null) {
      throw new IllegalArgumentException("clientId and clientSecret are required to create Gmail client.")
    }
    try {
      Credential credential = authorize(clientId, clientSecret)

      if (refreshToken != null && !refreshToken.empty) {
        credential.setRefreshToken(refreshToken)
      }
      if (accessToken != null && !accessToken.empty) {
        credential.setAccessToken(accessToken)
      }
      return new Gmail.Builder(transport, jsonFactory, setHttpTimeout(credential)).setApplicationName(applicationName).build()
    }
    catch (Exception e) {
      throw new RuntimeException("Could not create Gmail client.", e)
    }
  }

  // Authorizes the installed application to access user's protected data.
  private Credential authorize(String clientId, String clientSecret) throws Exception {
    // authorize
    return new GoogleCredential.Builder().setJsonFactory(jsonFactory).setTransport(transport).setClientSecrets(clientId, clientSecret).build()
  }

  @Override
  Message sendMessage(Gmail service, String userId, MimeMessage emailContent) throws MessagingException, IOException {
    Message message = createMessageWithEmail(emailContent)
    message = service.users().messages().send(userId, message).execute()

    return message
  }

  /**
   * Create a message from an email.
   *
   * @param emailContent Email to be set to raw of message
   * @return a message containing a base64url encoded email
   * @throws IOException
   * @throws MessagingException
   */
  @Override
  Message createMessageWithEmail(MimeMessage emailContent)
    throws MessagingException, IOException {
    ByteArrayOutputStream buffer = new ByteArrayOutputStream()
    emailContent.writeTo(buffer)
    byte[] bytes = buffer.toByteArray()
    String encodedEmail = Base64.encodeBase64URLSafeString(bytes)
    Message message = new Message()
    message.setRaw(encodedEmail)
    return message
  }

  /**
   * Create a MimeMessage using the parameters provided.
   *
   * @param to Email address of the receiver.
   * @param from Email address of the sender, the mailbox account.
   * @param subject Subject of the email.
   * @param bodyText Body text of the email.
   * @param file Path to the file to be attached.
   * @return MimeMessage to be used to send email.
   * @throws MessagingException
   */
  @Override
  MimeMessage createEmailWithAttachment(String to, String from, String subject, String bodyText, Map<String, byte[]> attachments) throws MessagingException, IOException {
    Properties props = new Properties()
    Session session = Session.getDefaultInstance(props, null)

    MimeMessage email = new MimeMessage(session)

    email.setFrom(new InternetAddress(from))
    email.addRecipient(javax.mail.Message.RecipientType.TO,
        new InternetAddress(to))
    email.setSubject(subject)

    MimeBodyPart mimeBodyPart = new MimeBodyPart()
    mimeBodyPart.setContent(bodyText, MIME_TYPE_GMAIL_TEST)

    Multipart multipart = new MimeMultipart()
    attachments.each { String fileName, byte[] content ->
      addAttachment(multipart, fileName, content, mimeBodyPart)
    }
    multipart.addBodyPart(mimeBodyPart)

    email.setContent(multipart)

    return email
  }


  HttpRequestInitializer setHttpTimeout(final HttpRequestInitializer requestInitializer) {
    // See: https://developers.google.com/api-client-library/java/google-api-java-client/errors
    return new HttpRequestInitializer() {
      @Override
      void initialize(HttpRequest httpRequest) throws IOException {
        requestInitializer.initialize(httpRequest)
        httpRequest.setConnectTimeout(connectTimeoutMillis)
        httpRequest.setReadTimeout(readTimeoutMillis)
      }
    }
  }


  private static void addAttachment(Multipart multipart, String fileName, byte[] fileData, MimeBodyPart mimeBodyPart) {
    mimeBodyPart = new MimeBodyPart()
    String fileMimeType = MIME_TYPE_GMAIL_TEST
    if (fileName ==~ /.*(zip|ZIP)/) {
      fileMimeType = MIME_TYPE_GMAIL_ZIP_TEST
    }
    DataSource source = new ByteArrayDataSource(fileData, fileMimeType)
    mimeBodyPart.setDataHandler(new DataHandler(source))
    mimeBodyPart.setFileName(fileName)
    multipart.addBodyPart(mimeBodyPart)
  }
}
