// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve


import groovy.transform.CompileStatic
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.SpringApplication
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.ApplicationContext
import org.springframework.context.annotation.Profile

import javax.annotation.Resource

@SpringBootApplication
@Slf4j
@Profile("default")
@CompileStatic 
class Application implements CommandLineRunner{

  private static final String SHOW_LABELS = 'showLabels'
  private static final String SHOW_EMAILS = 'showEmails'
  private static final String DOWNLOAD_ATTACHMENTS = 'downloadAttachments'

  @Autowired
  ApplicationContext context

  @Resource
  GoogleMailReader mailReader

  static void main(String[] args) {
    SpringApplication.run(Application, args)
  }

  @Override
  void run(String... args) throws Exception {

    if (args.size() == 0 || args[0] in ["help", "--help", "h"]) {
      help()
      return
    }
    log.info("Check parameters from args.")
    boolean showLabels = args.find { String arg -> arg == SHOW_LABELS } != null
    boolean showEmails = args.find { String arg -> arg == SHOW_EMAILS } != null
    boolean downloadAttachment = args.find { String arg -> arg == DOWNLOAD_ATTACHMENTS } != null

    List<String> labels = args.findAll { String arg -> arg != SHOW_EMAILS && arg != SHOW_LABELS && arg != DOWNLOAD_ATTACHMENTS } as List<String>

    mailReader.retrieveMessages(labels, showLabels, showEmails, downloadAttachment)
  }

  void help() {
    println("checkout the code, good luck")
  }
}
