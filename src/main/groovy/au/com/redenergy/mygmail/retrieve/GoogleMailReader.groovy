// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve

import com.google.api.client.repackaged.org.apache.commons.codec.binary.Base64
import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.Label
import com.google.api.services.gmail.model.ListLabelsResponse
import com.google.api.services.gmail.model.ListMessagesResponse
import com.google.api.services.gmail.model.Message
import com.google.api.services.gmail.model.MessagePart
import com.google.api.services.gmail.model.MessagePartBody
import com.google.api.services.gmail.model.MessagePartHeader
import groovy.util.logging.Slf4j
import org.springframework.beans.factory.annotation.Value
import org.springframework.stereotype.Component

import javax.annotation.PostConstruct
import java.time.LocalDateTime
import java.time.ZoneId

@Component
@Slf4j
class GoogleMailReader {

  private static LocalDateTime convertToLocalDateTimeViaInstant(Date dateToConvert) {
    return dateToConvert.toInstant()
        .atZone(ZoneId.systemDefault())
        .toLocalDateTime()
  }

  private static String getHeaderValue(List<MessagePartHeader> listOfHeaders, String value) {
    List<MessagePartHeader> headers = listOfHeaders.findAll { it.getName() == value }
    if (headers && !headers.isEmpty()) {
      headers.first().getValue()
    }
  }

  @Value('${gmail.attachment.retriever.poc.google.client.id}')
  String clientId

  @Value('${gmail.attachment.retriever.poc.google.client.secret}')
  String clientSecret

  @Value('${gmail.attachment.retriever.poc.google.access.token}')
  String accessToken

  @Value('${gmail.attachment.retriever.poc.google.refresh.token}')
  String refreshToken

  @Value('${gmail.attachment.retriever.poc.google.application.name}')
  String applicationName

  @Value('${gmail.attachment.retriever.poc.google.username}')
  String username

  @Value('${gmail.attachment.retriever.poc.google.mail.message.count}')
  String messageCount

  @Value('${gmail.attachment.retriever.poc.attachment.filter.expression}')
  String attachmentFilterExpression

  @Value('${gmail.attachment.retriever.poc.label.show}')
  Boolean defaultShowLabel

  @Value('${gmail.attachment.retriever.poc.emails.show}')
  Boolean defaultShowEmail

  GoogleMailClientFactory clientFactory = new BatchGoogleMailClientFactory()

  Gmail client

  Gmail makeClient() {
    return getClientFactory().makeClient(
        clientId,
        clientSecret,
        applicationName,
        refreshToken,
        accessToken)
  }

  @PostConstruct
  private void setup() {
    client = makeClient()
  }

  void retrieveMessages(List<String> labels) {
    retrieveMessages(labels, defaultShowLabel, defaultShowEmail, true)
  }

  void retrieveMessages(List<String> labels, Boolean showLabel, Boolean showEmail, Boolean downloadAttachment) {
    log.info("Retrieving messages (total: $messageCount).")

    if (showLabel) {
      ListLabelsResponse existingLabels = client
          .users()
          .labels()
          .list("${username}")
          .execute()
      log.info("Total labels: [${existingLabels.size()}]")
      existingLabels
          .getLabels()
          .each { Label label ->
            log.info("User [$username] name: [${label.name}], id: [${label.id}]")
          }
    }

    if (showEmail) {
      labels.each{ String label ->
        log.info("Processing label: [${label}]")

        processLabelAndRetryIfRequired(label, downloadAttachment)
      }
    }

    log.info("Email processing completed")
  }

  void processLabelAndRetryIfRequired(String label, boolean downloadAttachment) {
    boolean downloadSuccessful = false
    int attempt = 0
    while (!downloadSuccessful || attempt < 5) {
      try {
        processLabel(label, downloadAttachment)
        downloadSuccessful = true
        log.info("Attachements for label: [$label] successfully processed")
      }
      catch (IOException e) {
        log.error("Failed downloading label: [$label], try #[$attempt]",e)
      }
      attempt++
    }
  }

  void processLabel(String label, boolean downloadAttachment) {
    ListMessagesResponse listMessagesResponse = client
        .users()
        .messages()
        .list("${username}")
        .setLabelIds([label])
        .setMaxResults(Integer.valueOf(messageCount))
        .execute()
    List<Message> messages = listMessagesResponse.getMessages()
    log.info("Fetched total ${messages?.size()} from gmail mailbox. Processing attachments ...")

    if (downloadAttachment) {
      messages.each { Message message ->
        log.debug("Processing email: ${message}")
        processAttachmentIfAny(username, message.getId(), message)
      }
    }
  }
/**
   * Process attachment
   */
  private void processAttachmentIfAny(String userId, String messageId, Message originalMessage)
      throws IOException {
    Message message = client.users().messages().get(userId, messageId).execute()
    MessagePart payload = message.getPayload()
    List<MessagePartHeader> listOfHeaders = payload.getHeaders()
    LocalDateTime receivedDate = convertToLocalDateTimeViaInstant(new Date(message.getInternalDate()))
    String subject = getHeaderValue(listOfHeaders, 'Subject')
    String from = getHeaderValue(listOfHeaders, 'From')
    String body = message.getPayload()?.getBody()?.getData()

    List<MessagePart> parts = message.getPayload().getParts()
    List<GmailEmailAttachment> attachments = []
    for (MessagePart part : parts) {
      log.info("Checking attachment [${part.getFilename()}]")
      if (part.getFilename() != null && part.getFilename().length() > 0 && part.getFilename()  ==~ /${attachmentFilterExpression}/) {
        log.info("Processing email from: [${from}] with subject: [${subject}]. Body: [${body}], File name: [${part.getFilename()}] ")

        GmailEmailAttachment gmailEmailAttachment = new GmailEmailAttachment()
        gmailEmailAttachment.name = part.getFilename()
        gmailEmailAttachment.contentType = part.getMimeType()
        MessagePartBody attachPart = client.users().messages().attachments().get(userId, messageId, part.getBody().getAttachmentId()).execute()

        Base64 base64 = new Base64(true)
        byte[] fileByteArray = base64.decodeBase64(attachPart.getData())
        gmailEmailAttachment.data = fileByteArray
        attachments.add(gmailEmailAttachment)
      }
    }
    attachments
        .each { GmailEmailAttachment attachment ->
          String attachmentName = attachment.name
          byte[] content = attachment.data
          File persistedAttachment = new File("files/${attachmentName}")
          persistedAttachment.bytes = content
        }
  }

}
