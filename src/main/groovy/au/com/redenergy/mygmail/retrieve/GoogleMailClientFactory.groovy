// Copyright 2019 Red Energy Limited
// Pablo Sosa (psosa_ar@yahoo.com)
// 2019-08-21

package au.com.redenergy.mygmail.retrieve

import com.google.api.services.gmail.Gmail
import com.google.api.services.gmail.model.Message

import javax.mail.MessagingException
import javax.mail.internet.MimeMessage

interface GoogleMailClientFactory {

  /**
   * Create a message from an email.
   *
   * @param emailContent Email to be set to raw of message
   * @return a message containing a base64url encoded email
   * @throws IOException
   * @throws MessagingException
   */
  Message createMessageWithEmail(MimeMessage emailContent)
    throws MessagingException, IOException

  /**
   * Create a MimeMessage using the parameters provided.
   *
   * @param to Email address of the receiver.
   * @param from Email address of the sender, the mailbox account.
   * @param subject Subject of the email.
   * @param bodyText Body text of the email.
   * @param file Path to the file to be attached.
   * @return MimeMessage to be used to send email.
   * @throws MessagingException
   */
  MimeMessage createEmailWithAttachment(String to,
                                        String from,
                                        String subject,
                                        String bodyText,
                                        Map<String, byte[]> attachmentContent)
    throws MessagingException, IOException

  Gmail makeClient(String clientId, String clientSecret, String applicationName, String refreshToken, String accessToken)

  Message sendMessage(Gmail service, String userId, MimeMessage emailContent) throws MessagingException, IOException

}