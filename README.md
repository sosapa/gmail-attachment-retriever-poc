# Garp - Gmail Attachment Retriever Application
Connects to a Gmail account lists the emails and retrieve attachments based on a pattern

## Configuration

```bash

# Used for Ad-hoc job to retrieve the dispute resolution emails
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_CLIENT_ID=
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_CLIENT_SECRET=
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_ACCESS_TOKEN=
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_REFRESH_TOKEN=
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_APPLICATION_NAME=
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_USERNAME="username@redenergy.com.au"
export GMAIL_ATTACHMENT_RETRIEVER_POC_GOOGLE_MAIL_MESSAGE_COUNT="100"
export GMAIL_ATTACHMENT_RETRIEVER_POC_ATTACHMENT_FILTER_EXPRESSION="^NEM#DISPUTE.*(csv|CSV)$"

```                                                                 

## Execution

### Option one - run using springboot
```bash
gw clean bootRun
```           
### Option two - create the binary
```bash
gw clean assemble
```                                  

then, set following on your bash env
```bash

function garp() {
  java -jar $GMAIL_ATTACHMENT_RETRIEVER_HOME/build/libs/gmail-attachment-retriever-poc.jar $@
}

# run as follows

garp [showEmails] [showLabels] [downloadAttachments] [label_1 ... label_n]
```



Note email attachments are written to current directory.

```
# List of tried labels
//        "Label_161",
//        "Label_163",
//        "Label_167",
//        "Label_199",
//        "Label_209",
//        "Label_219",
//        "Label_225",
//        "Label_244",
//        "Label_252",
//        "Label_262",
//        "Label_264",
//        "Label_272",
//"Label_157",
//"Label_158",
//"Label_159",
//"Label_160",
////"Label_161",
//"Label_162",
////"Label_163",
//"Label_164",
//"Label_165",
//"Label_166",
////"Label_167",
//"Label_168",
//"Label_169",
//"Label_170",
//"Label_171",
//"Label_172",
//"Label_174",
//"Label_175",
//"Label_176",
//"Label_177",
//"Label_178",
//"Label_179",
//"Label_180",
//"Label_181",
//"Label_182",
//"Label_183",
//"Label_184",
//"Label_187",
//"Label_188",
//"Label_189",
//"Label_193",
//"Label_195",
//"Label_197",
////"Label_199",
//"Label_200",
//"Label_202",
//"Label_204",
//"Label_2049187130208731521",
//"Label_205",
//"Label_206",
//"Label_207",
//"Label_208",
//"Label_209",
//"Label_210",
//"Label_211",
//"Label_212",
//"Label_213",
//"Label_214",
//"Label_215",
//"Label_216",
//"Label_217",
//"Label_218",
////"Label_219",
//"Label_220",
//"Label_221",
//"Label_222",
//"Label_223",
//"Label_224",
////"Label_225",
//"Label_232",
//"Label_237",
//"Label_238",
//"Label_240",
//"Label_241",
//"Label_243",
////"Label_244",
//"Label_245",
//"Label_246",
//"Label_247",
//"Label_248",
//"Label_251",
////"Label_252",
//"Label_253",
//"Label_254",
//"Label_258",
//"Label_259",
//"Label_261",
////"Label_262",
//"Label_263",
//"Label_264",
//"Label_265",
//"Label_266",
//"Label_267",
//"Label_268",
//"Label_269",
//"Label_270",
//"Label_271",
//"Label_272",
//"Label_273",
//"Label_274",
//"Label_275",
//"Label_276",
//"Label_277",
//"Label_278",
//"Label_279",
//"Label_280",
//"Label_281",
//"Label_282",
//"Label_283",

Label_284 Label_288 Label_289 Label_290 Label_291 Label_292 Label_293 Label_3056043616844674084 Label_3313690144492119279 Label_4194291796752258558 Label_4608129119556319265

```